/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise2;


public class Point 
{
    private int x;
    private int y;
    
    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public String getPoint()
    {
        StringBuilder info = new StringBuilder();
        info.append("x: ");
        info.append(this.x);
        info.append(" y: ");
        info.append(this.y);
        return info.toString();
    }
}

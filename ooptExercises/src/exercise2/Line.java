/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise2;

import java.util.ListIterator;


public class Line extends Shape
{
    public Line()
    {
        super( 2, 3);   
    }
    
    public String getInfo()
    {
        StringBuilder info = new StringBuilder();
        info.append("LINE: \n");
        info.append("size: ");
        info.append(this.size);
        info.append("\n");
        info.append("corners: \n");
        for (final ListIterator<Point> i = this.corners.listIterator(); i.hasNext();)
        {
            final Point point = i.next();
            info.append(point.getPoint());
            info.append("\n");  
        }
        return info.toString();
    }
}
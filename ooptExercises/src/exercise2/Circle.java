/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise2;

public class Circle extends Shape
{
    public Circle()
    {
        super( 0, 7);   
    }
    
    public String getInfo()
    {
        StringBuilder info = new StringBuilder();
        info.append("CIRCLE: \n");
        info.append("size: ");
        info.append(this.size);
        info.append("\n");
        info.append("corners: \n");
        info.append("No corners :( \n");
 
        return info.toString();
    }
}
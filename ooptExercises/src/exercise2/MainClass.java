/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise2;


public class MainClass 
{
    public static void main(String[] args) 
    { 
       Rectangle rec = new Rectangle();
       Line line = new Line();
       Circle circ = new Circle();
       System.out.println(rec.getInfo());
       System.out.println(line.getInfo());
       System.out.println(circ.getInfo()); 
    }
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public abstract class Shape {
    
    private int numberOfCorners;
    protected int size;
    protected List<Point> corners = new ArrayList<>();

    
    public Shape(int nOC, int size)
    {
        this.numberOfCorners = nOC;
        this.size = size;
        
        Random generator = new Random();
        for(int i = 0; i < numberOfCorners; ++i)
        {
            int x = generator.nextInt(5) + 1;
            int y = generator.nextInt(5) + 1;
            
            this.corners.add(new Point(x, y));
        }
    }
    
    public abstract String getInfo();
}

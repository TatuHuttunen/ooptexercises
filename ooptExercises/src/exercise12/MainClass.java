/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise12;


public class MainClass {
    
    public static void main(String[] args) {
        
        Remote remote = new Remote();
        
        Light light = new Light();
        Cat cat = new Cat();
        GarageDoor gDoor = new GarageDoor();
        
        remote.setCommand1(new LightOnCommand(light));
        remote.setCommand2(new GarageDoorCommand(gDoor));
        remote.setCommand3(new CatCommand(cat));
        
        remote.button1WasPressed();
        remote.button2WasPressed();
        remote.button3WasPressed();
        
        /*
        Output:
        run:
        Button 1 was pressed
        Light was turned on
        Button 2 was pressed
        Garage door was opened
        Button 3 was pressed
        Cat has been activated
        BUILD SUCCESSFUL (total time: 0 seconds)
        */
    }
}

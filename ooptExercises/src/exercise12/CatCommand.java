/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise12;


public class CatCommand implements Command {
    private Cat cat;
    
    public CatCommand(Cat cat){
        this.cat = cat;
    }
    
    public void execute(){
        this.cat.activate();
    }
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise12;


public class Cat {
    public void activate() {
        System.out.println("Cat has been activated");
    }    
}

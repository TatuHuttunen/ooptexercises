/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise12;


public class LightOnCommand implements Command {
    private Light light;
    
    public LightOnCommand(Light light){
        this.light = light;
    }
    
    public void execute(){
        this.light.on();
    }
}

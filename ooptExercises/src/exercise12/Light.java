/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise12;


public class Light {
    public void on() {
        System.out.println("Light was turned on");
    }    
}

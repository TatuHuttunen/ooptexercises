/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise12;


public class Remote {
    private Command button1;
    private Command button2;
    private Command button3;
    
    public void setCommand1(Command command){
        this.button1 = command;
    }
    
    public void setCommand2(Command command){
        this.button2 = command;
    }
    
    public void setCommand3(Command command){
        this.button3 = command;
    }
    
    public void button1WasPressed(){
        System.out.println("Button 1 was pressed");
        this.button1.execute();
    }
    
    public void button2WasPressed(){
        System.out.println("Button 2 was pressed");
        this.button2.execute();
    }
    
    public void button3WasPressed(){
        System.out.println("Button 3 was pressed");
        this.button3.execute();
    }
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise12;


public class GarageDoor {
    public void open() {
        System.out.println("Garage door was opened");
    }    
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise12;


public class GarageDoorCommand implements Command {
    private GarageDoor gDoor;
    
    public GarageDoorCommand(GarageDoor gDoor){
        this.gDoor = gDoor;
    }
    
    public void execute(){
        this.gDoor.open();
    }
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise3;


public class Zebra extends Animal
{
    public Zebra (String name)
    {
        super (name, "Zebra");
    }
    
    public String getInfo()
    {
        StringBuilder info = new StringBuilder();
        info.append(this.species);
        info.append(" named ");
        info.append(this.name);
        info.append(" lives here.");
        info.append("ZEBRA! \n");
        return info.toString();
    }
}
/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise3;


public class Monkey extends Animal
{
    public Monkey (String name)
    {
        super (name, "Monkey");
    }
    
    public String getInfo()
    {
        StringBuilder info = new StringBuilder();
        info.append(this.species);
        info.append(" named ");
        info.append(this.name);
        info.append(" lives here.");
        info.append("BANANAS! \n");
        return info.toString();
    }
}

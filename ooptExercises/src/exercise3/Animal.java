/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise3;


public abstract class Animal 
{
    protected String name;
    protected String species;
    
    public Animal(String name, String species)
    {   
        this.name = name;
        this.species = species;
    }
    
    public void feed()
    {
        StringBuilder info = new StringBuilder();
        info.append("Fed a ");
        info.append(this.species);
        info.append(" named ");
        info.append(this.name);
        info.append("\n");
        System.out.println(info.toString());
    }
    
    public abstract String getInfo();
}

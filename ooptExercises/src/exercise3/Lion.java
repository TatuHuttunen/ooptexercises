/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise3;


public class Lion extends Animal
{
    public Lion (String name)
    {
        super (name, "Lion");
    }
    
    public String getInfo()
    {
       StringBuilder info = new StringBuilder();
        info.append(this.species);
        info.append(" named ");
        info.append(this.name);
        info.append(" lives here.");
        info.append("RAWR! \n");
        return info.toString();
    }
}

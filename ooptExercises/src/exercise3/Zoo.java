/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise3;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;


public class Zoo 
{
    private int numberOfCages;
    private List<Cage> cages = new ArrayList<>();
    
    public Zoo(int nOC)
    {
        this.numberOfCages =  nOC;
        for(int i = 0; i < this.numberOfCages; ++i)
        {           
            this.cages.add(new Cage());
        }
    }
    
    public String searchCageByNumber(int i)
    {
        return this.cages.get(i-1).animalInfo();
    }
    
    public void feedAnimals()
    {
        for (final ListIterator<Cage> i = this.cages.listIterator(); i.hasNext();)
        {
            final Cage cage = i.next();
            cage.feedAnimal();
        }
    }  
}

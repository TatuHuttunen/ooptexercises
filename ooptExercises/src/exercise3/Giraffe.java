/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise3;


public class Giraffe extends Animal
{
    public Giraffe (String name)
    {
        super (name, "Giraffe");
    }
    
    public String getInfo()
    {
        StringBuilder info = new StringBuilder();
        info.append(this.species);
        info.append(" named ");
        info.append(this.name);
        info.append(" lives here.");
        info.append("GIRAFFE SOUNDS! \n");
        return info.toString();
    }
}
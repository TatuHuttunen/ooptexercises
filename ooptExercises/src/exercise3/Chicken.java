/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise3;


public class Chicken extends Animal
{
    public Chicken (String name)
    {
        super (name, "Chicken");
    }
    
    public String getInfo()
    {
        StringBuilder info = new StringBuilder();
        info.append(this.species);
        info.append(" named ");
        info.append(this.name);
        info.append(" lives here.");
        info.append("kot kot kot \n");
        return info.toString();
    }
}
/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise3;


public class MainClass 
{
    public static void main(String[] args) 
    { 
       //construct zoo with 10 cages
       Zoo zoo = new Zoo(10);
       //feed the animals
       zoo.feedAnimals();
       //search cages 2, 5 and 7
       System.out.println(zoo.searchCageByNumber(2));
       System.out.println(zoo.searchCageByNumber(5));
       System.out.println(zoo.searchCageByNumber(7));
    }
}
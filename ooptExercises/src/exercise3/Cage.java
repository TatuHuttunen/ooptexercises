/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise3;

import java.util.Random;


public class Cage 
{
    private Animal animal;
    
    public Cage()
    {
        Random generator = new Random();
        int x = generator.nextInt(5) + 1;
        switch (x) 
        {
            case 1:  this.animal = new Lion("Leo");
                     break;
            case 2:  this.animal = new Giraffe("Geof");
                     break;
            case 3:  this.animal = new Zebra("Zoe");
                     break;
            case 4:  this.animal = new Monkey("George");
                     break;
            default: this.animal = new Chicken("Abraham");
                     break;
        
        }
    }
    
    public void feedAnimal()
    {
        this.animal.feed();
    }
    
    public String animalInfo()
    {
        return this.animal.getInfo();
    }
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise10;


public class Rectangle implements Shape {
    private int numberOfCorners = 4;
    private int size = 10;
    
    public int getNumberOfCorners(){
        return this.numberOfCorners;
    }
    
    public int getSize(){
        return this.size;
    }
}

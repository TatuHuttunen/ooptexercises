/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise10;


public class Circle implements Shape {
    private int numberOfCorners = 0;
    private int size = 8;
    
    public int getNumberOfCorners(){
        return this.numberOfCorners;
    }
    
    public int getSize(){
        return this.size;
    }
}

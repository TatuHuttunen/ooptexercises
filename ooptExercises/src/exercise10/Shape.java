/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise10;


public interface Shape {
    public int getNumberOfCorners();
    public int getSize();
}

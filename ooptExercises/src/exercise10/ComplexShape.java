/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise10;

import java.util.ArrayList;
import java.util.List;


public class ComplexShape implements Shape {    
    private List<Shape> shapes = new ArrayList<>();

    public void add(Shape shape){
        this.shapes.add(shape);
    }
    
    public int getNumberOfCorners(){
        int corners = 0;
        for (Shape shape : this.shapes) {
            corners += shape.getNumberOfCorners();
        }
        return corners;
    }
    
    public int getSize(){
        int size = 0;
        for (Shape shape : this.shapes) {
            size += shape.getSize();
        }
        return size;
    }      
}


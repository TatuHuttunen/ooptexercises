/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise10;


public class Triangle implements Shape {
    private int numberOfCorners = 3;
    private int size = 7;
    
    public int getNumberOfCorners(){
        return this.numberOfCorners;
    }
    
    public int getSize(){
        return this.size;
    }
}

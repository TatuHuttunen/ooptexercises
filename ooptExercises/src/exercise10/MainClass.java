/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise10;


public class MainClass {
    public static void main(String[] args) {
        ComplexShape cShape1 = new ComplexShape();
        ComplexShape cShape2 = new ComplexShape();

        cShape1.add(new Rectangle());
        cShape1.add(new Triangle());

        cShape2.add(new Rectangle());
        cShape2.add(new Triangle());
        cShape2.add(new Circle());

        System.out.println("cShape1 size: " + cShape1.getSize());
        System.out.println("cShape1 number of corners: " + cShape1.getNumberOfCorners());

        System.out.println("cShape2 size: " + cShape2.getSize());
        System.out.println("cShape2 number of corners: " + cShape2.getNumberOfCorners());
       
        /*
        Output:
        run:
        cShape1 size: 17
        cShape1 number of corners: 7
        cShape2 size: 25
        cShape2 number of corners: 7
        BUILD SUCCESSFUL (total time: 0 seconds)
        */
    } 
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise4;


public class Platypus extends Animal implements EggLaying, Running, Shouting
{
    public Platypus(String name)
    {
        super(name, "platypus");
    }
    
    public void layEggs()
    {
        System.out.println("platypus laying eggs");
    }
    
    public void run()
    {
        System.out.println("platypus running");
    }
    
    public void shout()
    {
        System.out.println("platypus shouting");
    }
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise4;


public class Pegasus extends Animal implements Running, Flying, Shouting
{
    public Pegasus(String name)
    {
        super(name, "Pegasus");
    }
    
    public void run()
    {
        System.out.println("Pegasus running");
    }
    
    public void fly()
    {
        System.out.println("Pegasus flying");
    }
    
    public void shout()
    {
        System.out.println("Pegasus shouting");
    }
}
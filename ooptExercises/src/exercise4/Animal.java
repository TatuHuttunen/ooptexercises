/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise4;

public abstract class Animal 
{
    protected String name;
    protected String species;
    
    public Animal(String name, String species)
    {   
        this.name = name;
        this.species = species;
    }
}

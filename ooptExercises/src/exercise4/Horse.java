/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise4;


public class Horse extends Animal implements Running, Shouting
{
    public Horse(String name)
    {
        super(name, "Horse");
    }
    
    public void run()
    {
        System.out.println("Horse running");
    }
    
    public void shout()
    {
        System.out.println("Horse shouting");
    }
}
/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise4;


public class MainClass 
{
    public static void main(String[] args) 
    {
       Platypus plat = new Platypus("plat");
       Horse hors = new Horse("horsie");
       Bird bird = new Bird("birdie");
       Pegasus pega = new Pegasus("pegax");
       Griffin grif = new Griffin("peter");
       
       plat.layEggs();
       plat.run();
       plat.shout();
       hors.run();
       hors.shout();
       bird.fly();
       bird.layEggs();
       bird.shout();
       pega.fly();
       pega.run();
       pega.shout();
       grif.fly();
       grif.run();
       grif.shout();
    }
}
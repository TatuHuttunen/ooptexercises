/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise4;


public class Bird extends Animal implements EggLaying, Flying, Shouting
{
    public Bird(String name)
    {
        super(name, "Bird");
    }
    
    public void layEggs()
    {
        System.out.println("Bird laying eggs");
    }
    
    public void fly()
    {
        System.out.println("Bird flying");
    }
    
    public void shout()
    {
        System.out.println("Bird shouting");
    }
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise4;


public class Griffin extends Animal implements Running, Flying, Shouting
{
    public Griffin(String name)
    {
        super(name, "Griffin");
    }
    
    public void run()
    {
        System.out.println("Griffin running");
    }
    
    public void fly()
    {
        System.out.println("Griffin flying");
    }
    
    public void shout()
    {
        System.out.println("Griffin shouting");
    }
}
/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise6;


public class MainClass 
{
    public static void main(String[] args) 
    {
        Character troll = new Troll();
        troll.setWeapon(new CubBehavior());
        System.out.println(troll.fight());
        troll.setWeapon(new AxeBehavior());
        System.out.println(troll.fight());
        Character queen = new Queen();
        queen.setWeapon(new KnifeBehavior());
        System.out.println(queen.fight());
    }
        /**
        Output:
        run:
        A troll is fighting Using cub as a weapon
        A troll is fighting Using axe as an weapon
        A queen is fighting Using knife as a weapon
        BUILD SUCCESSFUL (total time: 0 seconds)
        */
}

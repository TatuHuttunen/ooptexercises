/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise6;


public abstract class Character 
{
    protected WeaponBehavior weapon;
    
    public abstract String fight();
    
    public void setWeapon(WeaponBehavior w)
    {
        this.weapon = w;
    }
    
}

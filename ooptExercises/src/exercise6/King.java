/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise6;


public class King extends Character
{
    public String fight()
    {
        StringBuilder info = new StringBuilder();
        info.append("A king is fighting ");
        info.append(this.weapon.useWeapon());
        return info.toString();
    }
}

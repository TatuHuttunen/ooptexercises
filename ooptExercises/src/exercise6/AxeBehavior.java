/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise6;


public class AxeBehavior implements WeaponBehavior
{
    public String useWeapon()
    {
        return "Using axe as an weapon";
    }
}

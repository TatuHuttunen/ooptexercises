/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise6;


public interface WeaponBehavior 
{
    public String useWeapon();    
}

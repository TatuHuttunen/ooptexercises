/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise6;


public class Queen extends Character
{
    public String fight()
    {
        StringBuilder info = new StringBuilder();
        info.append("A queen is fighting ");
        info.append(this.weapon.useWeapon());
        return info.toString();
    }
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise6;


public class CubBehavior implements WeaponBehavior
{
    public String useWeapon()
    {
        return "Using cub as a weapon";
    }
}

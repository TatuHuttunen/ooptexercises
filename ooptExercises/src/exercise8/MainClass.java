/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise8;

import java.util.ArrayList;

public class MainClass 
{
    public static void main(String[] args) 
    {
        Deck deck = Deck.getInstance();
        ArrayList<Card> handPlayer1 = deck.deal(5);
         
        System.out.println("Hand of player 1:");
        for(Card card : handPlayer1){
            System.out.println(card.getInfo());
        }
        System.out.println("Cards left: " + deck.cardsLeft());
        System.out.println("");
        
        ArrayList<Card> handPlayer2 = deck.deal(5);  
        System.out.println("Hand of player 2:");
        for(Card card : handPlayer2){
            System.out.println(card.getInfo());
        }
        System.out.println("Cards left: " + deck.cardsLeft());
        System.out.println("");
        
        /** OUTPUT
        run:
        Hand of player 1:
        13 of Spades
        1 of Hearts
        10 of Spades
        12 of Spades
        1 of Clubs
        Cards left: 47

        Hand of player 2:
        6 of Clubs
        4 of Spades
        2 of Hearts
        11 of Hearts
        11 of Clubs
        Cards left: 42

        BUILD SUCCESSFUL (total time: 0 seconds)
        */
    }
}

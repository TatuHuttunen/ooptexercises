/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise8;


public class Card 
{
    private final String suit;
    private final int rank;
    
    public Card(String s, int r)
    {
        this.suit = s;
        this.rank = r;
    }
    
    public String getInfo()
    {
        StringBuilder str = new StringBuilder();
        str.append(this.rank);
        str.append(" of ");
        str.append(this.suit);
        return str.toString();
    }  
}

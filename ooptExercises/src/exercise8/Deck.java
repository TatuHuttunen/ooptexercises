/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


public class Deck 
{
    private static final Deck instance = new Deck();
    private final HashMap <String, Card> cards = new HashMap<>();
    
    private Deck()
    {
        //initialize the cards
        int  counter = 1;
        while (counter < 14)
        {
            String key = "D"+counter;
            cards.put(key, new Card("Diamonds", counter));
            counter++;
        }
        counter = 1;
        while (counter < 14)
        {
            String key = "H"+counter;
            cards.put(key, new Card("Hearts", counter));
            counter++;
        }
        counter = 1;
        while (counter < 14)
        {
            String key = "C"+counter;
            cards.put(key, new Card("Clubs", counter));
            counter++;
        }
        counter = 1;
        while (counter < 14)
        {
            String key = "S"+counter;
            cards.put(key, new Card("Spades", counter));
            counter++;
        }
        
    }
    
    public static Deck getInstance() 
    {
        return instance;
    }
    
    public int cardsLeft()
    {
        return this.cards.size();
    }
    
    public ArrayList deal(int nOC)
    {
        //check that there are enough cards 
        if (this.cardsLeft() >= nOC)
        {
            ArrayList<Card> cardsBeingDelt = new ArrayList();
            for (int i = 0; i < nOC; i++)
            {
                //Add random card to the hand
                Card card = this.getRandomCard();
                cardsBeingDelt.add(card);
            }
            return cardsBeingDelt;
        }
        //Return empty list if there is not enough cards left in the deck
        else 
            return new ArrayList();
    }
    
    private Card getRandomCard()
    {
        Random rand = new Random();
        int suitIndex = rand.nextInt(4);
        int rank = rand.nextInt(13) + 1;
        String[] suits = {"H", "D", "C", "S"};
        String key = suits[suitIndex] + rank;
        
        Card randCard = this.cards.get(key);
        if (randCard != null)
        {
            //delete dealt card from deck
            this.cards.remove(key);
            return randCard;
        }
        //If card was already deleted randomise a new one
        else
        {
            return this.getRandomCard();
        }
    }   
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise5;


public class Restaurant2Iterator implements Iterator{
    private Restaurant2 restaurant;
    private int counter = 0;
    
    public Restaurant2Iterator(Restaurant2 restaurant){
        this.restaurant = restaurant;
    }
    
    public Item next(){
        Item nxt = restaurant.menu.get(counter);
        this.counter++;
        return nxt;
    }
    
    public boolean hasNext(){
        return (this.counter < restaurant.menu.size());
    }
    
}

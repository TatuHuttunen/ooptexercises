/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise5;


public class Item {
    private final String name;
    private final String description;
    private final int price;
    
    public Item(String name, String description, int price){
        this.name = name;
        this.description = description;
        this.price = price;
    }
    
    public String getInfo(){
        StringBuilder info = new StringBuilder();
        info.append("Name: ");
        info.append(this.name);
        info.append(", description: ");
        info.append(this.description);
        info.append(", price: ");
        info.append(this.price);
        return info.toString();
    }
}

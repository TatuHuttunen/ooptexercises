/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise5;


public class MainClass 
{
    public static void main(String[] args) 
    {
       Iterator[] iterators = new Iterator[2];
       Restaurant1 r1 = new Restaurant1();
       Restaurant2 r2 = new Restaurant2();
       iterators[0] = r1.createIterator();
       iterators[1] = r2.createIterator();

       for(Iterator iterator : iterators){
           while (iterator.hasNext()){
               System.out.println(iterator.next().getInfo());
           }
       }
       
       /**
        Output:
          run:
          Name: Salad, description: good for you!, price: 20
          Name: Pork, description: with potatoes, price: 13
          Name: Ice cream, description: makiaa, price: 11
          Name: fish, description: slimy, price: 10
          Name: chicken, description: chickeny, price: 15
          Name: Beef, description: ground, price: 12
          BUILD SUCCESSFUL (total time: 0 seconds)
        */
    } 
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise5;


public interface Iterative {
    public Iterator createIterator();
}

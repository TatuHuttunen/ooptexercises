/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise5;


public class Restaurant1Iterator implements Iterator{
    private final Restaurant1 restaurant;
    private int counter = 0;
    
    public Restaurant1Iterator(Restaurant1 restaurant){
        this.restaurant = restaurant;
    }
    
    public Item next(){
        Item nxt = restaurant.menu[counter];
        this.counter++;
        return nxt;
    
    }
    
    public boolean hasNext(){
        return (this.counter < restaurant.menu.length);
    }
    
}

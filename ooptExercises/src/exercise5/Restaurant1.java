/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise5;

public class Restaurant1 implements Iterative {
    Item[] menu;
    
    public Restaurant1(){
        menu = new Item[3];
        menu[0] = new Item("Salad", "good for you!", 20);
        menu[1] = new Item("Pork", "with potatoes", 13);
        menu[2] = new Item("Ice cream", "makiaa", 11); 
    }
    
    public Restaurant1Iterator createIterator(){
        return new Restaurant1Iterator(this);
    }
    
}

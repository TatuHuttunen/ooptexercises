/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise5;

import java.util.ArrayList;


public class Restaurant2 implements Iterative {
    ArrayList<Item> menu;
    
    public Restaurant2(){
        this.menu = new ArrayList<Item>();
        this.menu.add(new Item("fish", "slimy", 10));
        this.menu.add(new Item("chicken", "chickeny", 15));
        this.menu.add(new Item("Beef", "ground", 12));
    }
    
    public Restaurant2Iterator createIterator(){
        return new Restaurant2Iterator(this);
    } 
    
}

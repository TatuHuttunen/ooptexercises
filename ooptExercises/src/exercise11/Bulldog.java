/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise11;


public class Bulldog implements Dog{
    
    public String bark(){
        return("Bulldog barking");
    }
}

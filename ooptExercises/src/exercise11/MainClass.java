/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise11;


public class MainClass {
    
    public static void main(String[] args) {
        
        Factory factory = new Factory();
        
        Dog dog1 = factory.createDog("bulldog");
        Dog dog2 = factory.createDog("germanshepard");
        Dog dog3 = factory.createDog("labrador");
        
        System.out.println(dog1.bark());
        System.out.println(dog2.bark());
        System.out.println(dog3.bark());
        
        /*
        Output:
        run:
        Bulldog barking
        German shepard barking
        Labrador barking
        BUILD SUCCESSFUL (total time: 0 seconds)
        */
    }
}

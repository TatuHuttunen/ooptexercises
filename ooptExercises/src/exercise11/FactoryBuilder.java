/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise11;


public abstract class FactoryBuilder {
    public abstract Dog createDog(String breed);
}

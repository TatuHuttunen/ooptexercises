/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise11;


public class Factory extends FactoryBuilder {
    
    public Factory(){
        super();
    }
    
    public Dog createDog(String breed) {
        if(breed == null) {
            return null;
        }		
        if(breed.equalsIgnoreCase("BULLDOG")) {
            return new Bulldog();

        }
        else if(breed.equalsIgnoreCase("LABRADOR")) {
            return new Labrador();

        }
        else if(breed.equalsIgnoreCase("GERMANSHEPARD")) {
            return new GermanShepard();
        }

        return null;
    }
}

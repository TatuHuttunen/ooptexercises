/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise1;

import java.util.Date;


public class MainClass 
{
    public static void main(String[] args) 
    { 
       Date start = new Date();
       Date end = new Date();
       long rentalLength = 1000000000L;
       end.setTime(start.getTime()+ rentalLength);
       
       Car corolla = new Car("Corolla", "Toyota");
       Customer matt = new Customer("Matt", "123456-654F");
       
       Rental rental = new Rental(corolla, matt, start, end);
       System.out.println(rental.getInfo());
    } 
}

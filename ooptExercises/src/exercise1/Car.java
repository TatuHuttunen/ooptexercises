/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise1;

import java.util.Random;


public class Car 
{
    private int id;
    private String model;
    private String manufacturer;
    
    public Car(String model, String manufacturer)
    {
        Random generator = new Random(); 
        this.id = generator.nextInt(9999) + 1;
        this.model = model;
        this.manufacturer = manufacturer;

    }
    
    public String getInfo()
    {
        StringBuilder info = new StringBuilder();
        info.append("ID: ");
        info.append(this.id);
        info.append(" Model: ");
        info.append(this.model);
        info.append(" manufacturer: ");
        info.append(this.manufacturer);
        return info.toString();
    } 
}

/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise1;

import java.util.Random;


public class Customer 
{
    private int id;
    private String name;
    private String socialSecurity;
    
    public Customer(String name, String socialSecurity)
    {
        Random generator = new Random(); 
        this.id = generator.nextInt(9999) + 1;
        this.name = name;
        this.socialSecurity = socialSecurity;

    }
    
    public String getInfo()
    {
        StringBuilder info = new StringBuilder();
        info.append("ID: ");
        info.append(this.id);
        info.append(" Name: ");
        info.append(this.name);
        info.append(" SSN: ");
        info.append(this.socialSecurity);
        return info.toString();
    } 
    
}

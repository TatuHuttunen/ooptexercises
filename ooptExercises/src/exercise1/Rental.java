/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise1;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Rental 
{
    private Car car;
    private Customer customer;
    private Date start;
    private Date end;
    
    public Rental(Car car, Customer customer, Date start, Date end)
    {
        this.car = car;
        this.customer = customer;
        this.start = start;
        this.end = end;
    }
    
    public String getInfo()
    {
        String starDate = new SimpleDateFormat("dd.MM.").format(start);
        String endDate = new SimpleDateFormat("dd.MM.").format(end);
        
        StringBuilder info = new StringBuilder();
        info.append("RENTAL INFO:\n");
        info.append("Start Date: ");
        info.append(starDate);
        info.append("\n");
        info.append("End Date: ");
        info.append(endDate);
        info.append("\n");
        info.append("Car: ");
        info.append(this.car.getInfo());
        info.append("\n");
        info.append("Customer: ");
        info.append(this.customer.getInfo());
        return info.toString();
    } 
}

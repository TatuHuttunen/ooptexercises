/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise7;


public class ShapeFillColor implements Shape
{
    protected Shape shape;
    
    public ShapeFillColor (Shape shape)
    {
        this.shape = shape;
    }
    public String draw()
    {
        return shape.draw();
    }
}

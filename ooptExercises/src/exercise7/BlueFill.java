/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise7;


public class BlueFill extends ShapeFillColor
{
    
    public BlueFill (Shape shape)
    {
        super(shape);
    }
    
    private String setBlueFill()
    {
      return "Fill Color: Blue";
    }
    
    public String draw()
    {
        StringBuilder str = new StringBuilder();
        str.append(shape.draw());
        str.append("\n");
        str.append(this.setBlueFill());
        return str.toString();
    }
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise7;


public class RedFill extends ShapeFillColor
{
    
    public RedFill (Shape shape)
    {
        super(shape);
    }
    
    private String setRedFill()
    {
      return "Fill Color: Red";
    }
    
    public String draw()
    {
        StringBuilder str = new StringBuilder();
        str.append(shape.draw());
        str.append("\n");
        str.append(this.setRedFill());
        return str.toString();
    }
}

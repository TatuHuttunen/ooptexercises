/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise7;


public class Rectangle implements Shape
{
    public String draw()
    {
        return "Shape: Rectangle";
    }
}

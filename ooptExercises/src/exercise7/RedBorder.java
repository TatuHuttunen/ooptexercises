/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise7;


public class RedBorder extends ShapeBorderColor
{
    
    public RedBorder (Shape shape)
    {
        super(shape);
    }
    
    private String setRedBorder()
    {
      return "Border Color: Red";
    }
    
    public String draw()
    {
        StringBuilder str = new StringBuilder();
        str.append(shape.draw());
        str.append("\n");
        str.append(this.setRedBorder());
        return str.toString();
    }
}

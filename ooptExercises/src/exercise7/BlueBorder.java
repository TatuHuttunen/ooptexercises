/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise7;


public class BlueBorder extends ShapeBorderColor
{
    
    public BlueBorder (Shape shape)
    {
        super(shape);
    }
    
    private String setBlueBorder()
    {
      return "Border Color: Blue";
    }
    
    public String draw()
    {
        StringBuilder str = new StringBuilder();
        str.append(shape.draw());
        str.append("\n");
        str.append(this.setBlueBorder());
        return str.toString();
    }
}

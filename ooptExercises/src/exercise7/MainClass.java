/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise7;


public class MainClass 
{
    public static void main(String[] args) 
    {
        Shape line = new Line();
        Shape circleRedBorder = new RedBorder(new Circle());
        Shape rectangleRedBorderBlueFill = new BlueFill(new RedBorder(new Rectangle()));
        
        System.out.println("Simple Line:");
        System.out.println(line.draw());
        System.out.println("");
        
        System.out.println("Circle with red border:");
        System.out.println(circleRedBorder.draw());
        System.out.println("");
        
        System.out.println("Rectangle, red border, blue fill:");
        System.out.println(rectangleRedBorderBlueFill.draw());
        System.out.println("");
        
        /** OUTPUT
        run:
        Simple Line:
        Shape: Line

        Circle with red border:
        Shape: Circle
        Border Color: Red

        Rectangle, red border, blue fill:
        Shape: Rectangle
        Border Color: Red
        Fill Color: Blue

        BUILD SUCCESSFUL (total time: 0 seconds)
        */

    }
}

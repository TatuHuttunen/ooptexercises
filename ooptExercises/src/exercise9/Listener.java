/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise9;


public class Listener implements Observer {
    private String name;
    
    public Listener (String name){
        this.name = name;
    }
    
    public void update(String post){
        System.out.println( "Listener: " + this.name + " Post: " + post);
    }
}

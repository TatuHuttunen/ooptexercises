/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise9;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;


public class User extends Thread implements Subject{
    private Thread t;
    private Observer observer;
    private String name;
    private String message;
    private Scanner scanner;
    
    public User(String name, String filename){
        this.name = name;
        try {
            File file = new File(filename);
            this.scanner = new Scanner(file);
            scanner.useDelimiter(System.getProperty("line.separator"));
        } catch(FileNotFoundException fnfe) { 
        }
    }
    
    
    public void registerObserver(Observer observer){
        this.observer = observer;
    }
    
    public void removeObserver(){
        this.observer = null;
    }
    
    public void notifyObserver(){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        String post = " " + this.message + " From: " + this.name + " " + dateFormat.format(date);
        this.observer.update(post);
    }
    
    public void start (){
      if (t == null){
         t = new Thread (this, this.name);
         t.start ();
      }
   }
   
   public void run() {
      try {
         for(int i = 0; i < 3; i++) {
            this.message = this.readFile();
            this.notifyObserver();
            Thread.sleep(3000);
         }
     } catch (InterruptedException e) {
         System.out.println(this.name + " interrupted");
     }
     System.out.println(this.name + " closing");
   }
   
    private String readFile(){
        if (this.scanner.hasNext()){
            return this.scanner.next();
        }
        return "";
    }
}

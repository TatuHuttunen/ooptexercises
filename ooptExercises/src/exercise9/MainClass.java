/* CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409
 */
package exercise9;

public class MainClass {
    public static void main(String[] args) {
        //file paths
        String file1 = "C:\\Users\\Tatu\\Repos\\oopt\\ooptExercises\\src\\exercise9\\file1.txt";
        String file2 = "C:\\Users\\Tatu\\Repos\\oopt\\ooptExercises\\src\\exercise9\\file2.txt";
        String file3 = "C:\\Users\\Tatu\\Repos\\oopt\\ooptExercises\\src\\exercise9\\file3.txt";

        //create users
        User user1 = new User("Jokke", file1);
        User user2 = new User("Pekka", file2);
        User user3 = new User("Kari", file3);

        //create listeners
        Listener listener1 = new Listener("Listener1");
        Listener listener2 = new Listener("Listener2");
        Listener listener3 = new Listener("Listener3");

        user1.registerObserver(listener1);
        user1.start();

        user2.registerObserver(listener2);
        user2.start();

        user3.registerObserver(listener3);
        user3.start();
       
        /*
        Output:
        run:
        Listener: Listener1 Post:  Lorem ipsum dolor sit amet, id nam modo animal. From: Jokke 00:51:47
        Listener: Listener2 Post:  Ne omnes delicata dissentiet vix, qui errem docendi cu, duo placerat corrumpit id. From: Pekka 00:51:47
        Listener: Listener3 Post:  At vim primis deseruisse, eu laudem facilisi democritum sed.  From: Kari 00:51:47
        Listener: Listener1 Post:  Dicunt menandri mel cu, atqui animal eu ius.  From: Jokke 00:51:50
        Listener: Listener3 Post:  Mel ne unum tincidunt consequuntur, sea dicat decore no.  From: Kari 00:51:50
        Listener: Listener2 Post:  Pri an evertitur vituperata, ei ius tritani corpora delicata.  From: Pekka 00:51:50
        Listener: Listener2 Post:  Ei usu unum ponderum accusamus, in per diceret reformidans.  From: Pekka 00:51:53
        Listener: Listener3 Post:  Mel ut porro nullam definiebas, et has nusquam imperdiet vulputate.  From: Kari 00:51:53
        Listener: Listener1 Post:  Illum vivendo explicari in vix, case adipiscing et quo. From: Jokke 00:51:53
        Kari closing
        Jokke closing
        Pekka closing
        BUILD SUCCESSFUL (total time: 9 seconds)
        */
    } 
}

/*
 * CT60A7500 Object-Oriented Programming Techniques
 * Tatu Huttunen, 0358409}
 */
package exercise9;


interface Subject {
    public void notifyObserver();
    public void registerObserver(Observer observer);
    public void removeObserver();
}
